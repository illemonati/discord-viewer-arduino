#include "pitches.h"


constexpr int RED_LED = 7;
constexpr int BLUE_LED = 6;
constexpr int GREEN_LED = 5;
constexpr int YELLOW_LED = 4;

constexpr int BAUD_RATE = 9600;
constexpr int BUZZER_PIN = 8;
constexpr int MUTE_BUTTON = 13;  
constexpr int melodies[][4] = {{NOTE_E6, NOTE_D6, NOTE_C6, NOTE_G5},
                               {NOTE_C6, NOTE_E6, NOTE_D6, NOTE_G5},
                               {NOTE_C6, NOTE_D6, NOTE_E6, NOTE_C6},
                               {NOTE_E6, NOTE_C6, NOTE_D6, NOTE_G5},
                               {NOTE_G5, NOTE_D6, NOTE_E6, NOTE_C6},
                               {NOTE_E6, NOTE_D6, NOTE_C6, NOTE_G5},
                               {NOTE_C6, NOTE_E6, NOTE_D6, NOTE_G5},
                               {NOTE_C6, NOTE_D6, NOTE_E6, NOTE_C6},
                               {NOTE_E6, NOTE_C6, NOTE_D6, NOTE_G5},
                               {NOTE_G5, NOTE_D6, NOTE_E6, NOTE_C6},
                               {NOTE_C5}};
constexpr int duration = 100;

bool mute = false;
bool holdingDown = false;

void alertSound() {

    if (mute) {
        return;
    }
  
    for (auto& melody : melodies) {
        for (auto& note : melody) {
            int led = RED_LED;

            switch (note) {
                case NOTE_G5:
                    led = RED_LED;
                    break;
                case NOTE_C6:
                    led = BLUE_LED;
                    break;
                case NOTE_D6:
                    led = GREEN_LED;
                    break;
                case NOTE_E6:
                    led = YELLOW_LED;
                    break;
            }
            
            digitalWrite(led, HIGH);
            tone(BUZZER_PIN, note, duration);
            delay(duration * 2);
            digitalWrite(led, LOW);
            
        }
        delay(duration * 2 * 3);
    }
}



void setup() {
    pinMode(RED_LED, OUTPUT);
    pinMode(MUTE_BUTTON, INPUT);
    digitalWrite(RED_LED, LOW);
    Serial.begin(BAUD_RATE);
    delay(1000);
}

void loop() {

    if (mute) {
        digitalWrite(BLUE_LED, HIGH);
    } else {
        digitalWrite(BLUE_LED, LOW);
    }

    if (holdingDown) {
        digitalWrite(GREEN_LED, HIGH);
    } else {
        digitalWrite(GREEN_LED, LOW);
    }

    if ((digitalRead(MUTE_BUTTON) == HIGH) && !holdingDown) {
        mute = !mute;
        holdingDown = true;
        digitalWrite(RED_LED, HIGH);
        delay(1000);
        digitalWrite(RED_LED, LOW);
    } else if (digitalRead(MUTE_BUTTON) == LOW) {
        holdingDown = false;
    }
  
    if (Serial.available()) {
        alertSound();
        Serial.readString();
    }
}
