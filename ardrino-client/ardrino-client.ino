

#include <LiquidCrystal.h>
#include <SoftwareSerial.h>

#include "pitches.h"

constexpr uint32_t SCREEN_WIDTH = 16;
constexpr uint32_t SCREEN_LENGTH = 2;
constexpr uint32_t SW_pin = 4;
constexpr uint32_t X_pin = 0;
constexpr uint32_t Y_pin = 1;
constexpr uint32_t BAUD_RATE = 9600;
constexpr uint32_t BACKLOG_SIZE = 20;

LiquidCrystal lcd(7, 8, 9, 10, 11, 12);
SoftwareSerial mySerial(2, 3);  // RX, TX

uint32_t currentPage = 0;

class Message {
   public:
    String * messageContent = nullptr;
    uint32_t maxPage = 0;
    uint32_t messageCount = 0;

    Message(String content, uint32_t messageCount) {
        this->messageCount = messageCount;
        messageContent =
            new String("[" + String(this->messageCount) + "]" + " " + content);
        this->maxPage = calcMaxPage();
    }

    ~Message() {
        delete messageContent;
    }

    uint32_t calcMaxPage() {
        return (messageContent->length() - 1) / SCREEN_LENGTH / SCREEN_WIDTH;
    }



    void acknowlege() {
        Serial.print("acknowlege ");
        Serial.print(" ");
        Serial.print(this->messageCount);
        Serial.print("\n");
    }

    void print() {
        lcd.clear();

        if (!this->messageContent) {
            return;
        }

        uint32_t startIndex = currentPage * SCREEN_LENGTH * SCREEN_WIDTH;
        if (startIndex > this->messageContent->length() - 1) {
            return;
        }

        for (uint32_t i = 0; i < SCREEN_LENGTH; i++) {
            lcd.setCursor(0, i);
            for (uint32_t j = 0; j < SCREEN_WIDTH; j++) {
                uint32_t index = i * SCREEN_WIDTH + j + startIndex;
                if (index > this->messageContent->length() - 1) {
                    return;
                }
                lcd.print(this->messageContent->charAt(index));
            }
        }
    }
};

class MessageLink {
   public:
    Message* message = nullptr;
    MessageLink* previous = nullptr;
    MessageLink* next = nullptr;
    MessageLink(Message* message) { this->message = message; }
    MessageLink(Message* message, MessageLink* previous)
        : MessageLink(message) {
        this->previous = previous;
    }
    MessageLink(Message* message, MessageLink* previous, MessageLink* next)
        : MessageLink(message, previous) {
        this->next = next;
    }
    ~MessageLink() {
        delete message;
    }
};

class Messages {
   public:
    MessageLink* head = nullptr;
    MessageLink* currentMessage = nullptr;
    MessageLink* tail = nullptr;
    uint32_t totalMessageCount = 0;
    uint32_t numberOfMessages = 0;
    Messages() {
        head = nullptr;
        tail = nullptr;
        currentMessage = nullptr;
        numberOfMessages = 0;
        totalMessageCount = 0;
    }
    ~Messages() {
        MessageLink* cursor = head;
        while(cursor) {
            MessageLink* temp = cursor->next;
            delete cursor;
            cursor = temp;
        }
    }
    void pushMessage(String msg) {
        numberOfMessages++;
        totalMessageCount++;
        Message* newMessage = new Message(msg, totalMessageCount-1);
        MessageLink* newMessageLink = new MessageLink(newMessage, this->tail);
        tail->next = newMessageLink;
        tail = newMessageLink;
        if (!head) {
            head = tail;
        } else if (numberOfMessages > BACKLOG_SIZE) {
            head = head->next;
            delete head->previous;
            head->previous = nullptr;
        }
        currentMessage = tail;
        currentPage = 0;
    }
    void foward() {
        if (!currentMessage || !currentMessage->next) {
            return;
        }
        currentMessage = currentMessage->next;
    }
    void back() {
        if (!currentMessage || !currentMessage->previous) {
            return;
        }
        currentMessage = currentMessage->previous;
    }
};

Messages messages;

enum JoyStickCommand {
    PreviousPage,
    NextPage,
    PreviousMessage,
    NextMessage,
    AcknowlegeMessage,
    None
};

void monitorJoyStickCommand() {
    int joyStickX = analogRead(X_pin);
    int joyStickY = analogRead(Y_pin);
    JoyStickCommand command = JoyStickCommand::None;
    if (!digitalRead(SW_pin)) {
        command = JoyStickCommand::AcknowlegeMessage;
    } else if (joyStickX > 550) {
        command = JoyStickCommand::NextPage;
    } else if (joyStickX < 450) {
        command = JoyStickCommand::PreviousPage;
    } else if (joyStickY > 550) {
        command = JoyStickCommand::NextMessage;
    } else if (joyStickY < 450) {
        command = JoyStickCommand::PreviousMessage;
    };

    switch (command) {
        case JoyStickCommand::AcknowlegeMessage:
            messages.currentMessage->message->acknowlege();
            break;
        case JoyStickCommand::NextPage:
            Serial.print("nextPage\n");
            if (currentPage < messages.currentMessage->message->maxPage) {
                currentPage++;
                messages.currentMessage->message->print();
            }
            break;
        case JoyStickCommand::PreviousPage:
            Serial.print("previousPage\n");
            if (currentPage > 0) {
                currentPage--;
                messages.currentMessage->message->print();
            }
            break;
        case JoyStickCommand::NextMessage:
            Serial.print("nextMesage\n");
            if (messages.currentMessage->next) {
                currentPage = 0;
                messages.foward();
                messages.currentMessage->message->print();
            }
            break;
        case JoyStickCommand::PreviousMessage:
            Serial.print("previousMesage\n");
            if (messages.currentMessage->previous) {
                messages.back();
                messages.currentMessage->message->print();
            }
            break;
    }
}

void setup() {
    pinMode(SW_pin, INPUT);
    digitalWrite(SW_pin, HIGH);
    Serial.begin(BAUD_RATE);
    lcd.begin(16, 2);
    messages.pushMessage("Welcome To Discord!");
    messages.currentMessage->message->print();
    mySerial.begin(BAUD_RATE);
    while (!Serial) {
    }
}

void loop() {
    if (Serial.available()) {
        messages.pushMessage(Serial.readString());
        mySerial.write(12);
        messages.currentMessage->message->print();
    }

    monitorJoyStickCommand();

    delay(100);
}
