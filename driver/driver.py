import serial
import time
import discord
import os
import asyncio
from collections import deque


BAUD_RATE = 9600
COM_PORT = "COM8"

APPROVED_USERS = [
    313687614853218306,
    443616025674776576,
    457222651773976597
]


arduino = serial.Serial(
    COM_PORT,
    BAUD_RATE,
    bytesize=serial.EIGHTBITS,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    timeout=1,
    xonxoff=0,
    rtscts=0
)

try:
    arduino.open()
    arduino.flushInput()
    arduino.setDTR(True)
except Exception as e:
    pass


messages = deque(maxlen=20)
messages.append((0, None))

class MyClient(discord.Client):
    async def on_ready(self):
        print('Logged on as', self.user)

    async def on_message(self, message):
        if message.author.id in APPROVED_USERS:
            messages.append((messages[len(messages)-1][0]+1, message))
            arduino.write(message.clean_content.encode())


async def listen_for_client_command():
    while True:
        commands = arduino.read_all().decode().split('\n')
        for command in commands:
            if len(command) > 0:
                print(command)
            if command.startswith('acknowlege '):
                messageNumber = int(command[len('ackowlege '):])
                message = [val for (key, val) in messages if key == messageNumber][0]
                if message is not None:
                    await message.channel.send("Acknowleged")
        await asyncio.sleep(0.5)


async def main():
    client = MyClient()
    client.loop.create_task(listen_for_client_command())
    await client.login(os.environ.get('DISCORD_TOKEN'), bot=False)
    await client.connect()


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()